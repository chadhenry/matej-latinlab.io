---
layout: resource
title:  "Beautiful Web Type"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: Chad Mazzola
link: https://beautifulwebtype.com/
description: A showcase of the best typefaces from Google Fonts.
categories: 
  - Resources 
  - Combining-Fonts
tags:
  - Gallery
  - Tool
---

{{ page.author }}
