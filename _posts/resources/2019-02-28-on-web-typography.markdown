---
layout: resource
title:  "On Web Typography"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: Jason Santa Maria
link: https://player.vimeo.com/video/34178417
description: Through an understanding of our design tools and how they relate to the web as a medium, we can empower ourselves to use type in meaningful and powerful ways.
categories: 
  - Resources 
  - Basics
tags:
  - Talk
  - Video
---

{{ page.author }}
