---
layout: resource
title:  "FontFace Ninja"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: fontface.ninja
link: https://www.fontshop.com/glossary
description: Fontface Ninja is a browser extension that lets you inspect, try, bookmark and buy fonts on any websites.
categories: 
  - Resources 
  - Recognizing-Choosing-Fonts
tags:
  - Plugin
  - Chrome
---

{{ page.author }}
