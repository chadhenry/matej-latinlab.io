---
layout: resource
date: 2019-11-15 16:57:09 +0100
exclude: true
title: fonttools
author: FontTools
link: https://github.com/fonttools/fonttools
description: A library to manipulate font files from Python.
categories:
- Resources
- Font-Tools
tags:
- Tool

---
