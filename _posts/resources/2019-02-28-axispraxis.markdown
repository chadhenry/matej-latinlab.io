---
layout: resource
title:  "AxisPraxis"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: Laurence Penney
link: https://www.axis-praxis.org/specimens/__DEFAULT__
description: A website for playing with OpenType variable fonts.
categories: 
  - Resources 
  - Variable-Fonts
tags:
  - Tool
  - Frontend
---

{{ page.author }}
