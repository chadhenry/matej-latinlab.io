---
layout: resource
title:  "Typography Inspector"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: Bram Stein
link: https://chrome.google.com/webstore/detail/typography-inspector/mbbnlbimlnfachlfnjnolpehiimhbjjm
description: Typography Inspector analyses the typography on your site and helps you improve it.
categories: 
  - Resources 
  - Basics
tags:
  - Plugin
  - Chrome
---

{{ page.author }}
