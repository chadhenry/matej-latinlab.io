---
layout: resource
title:  "Web Font Previewer"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: JustinTheClouds
link: https://chrome.google.com/webstore/detail/webfont-previewer/ehmpabgeehikhdodemjoenbonjkdeopn
description: This extension allows you to test webfonts out on any website and target which elements the fonts should be applied to.
categories: 
  - Resources 
  - Recognizing-Choosing-Fonts
tags:
  - Plugin
  - Chrome
---

{{ page.author }}
