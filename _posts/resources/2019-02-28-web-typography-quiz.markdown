---
layout: resource
title:  "Web Typography Quiz"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: Matej Latin
link: /web-typography-quiz/
description: Web typography is a bit tricky. Take this web typography quiz and test your typography knowledge.
image: /assets/img/resources/bwt-quiz@2x.jpg
categories: 
  - Resources 
  - Better-Web-Type
tags:
  - Game
---

{{ page.author }}
