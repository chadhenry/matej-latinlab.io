---
layout: resource
title:  "TypeWonder"
date:   2019-02-28 16:46:56 +0000
exclude: true
author: typewonder.com
link: https://chrome.google.com/webstore/detail/typewonder/ohgmapelghofmbacalgamfbejaghdilh
description: This extension helps you to test webfonts on active chrome tab via typewonder.com.
categories: 
  - Resources 
  - Recognizing-Choosing-Fonts
tags:
  - Plugin
  - Chrome
---

{{ page.author }}
