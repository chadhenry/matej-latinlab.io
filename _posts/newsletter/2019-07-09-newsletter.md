---
layout: post-newsletter
title: Cool new stuff in web typography in July ’19
title-short: July ’19
categories:
- Newsletter
tags:
- Free-Font
date: 2019-07-09 16:06:57 +0200
author: matej
toc: true
intro: |2-

  Gangster Grotesk is the featured free font of this edition, we take a look at using vertical rhythm in Sketch, optimizing Google Fonts performance and the new font loading strategy for CSS Tricks.
edition: '011'
image: "/assets/img/newsletter/011/post@2x.jpg"

---
How is it going? It seems like pretty much everyone around me is on holiday but I’m still here working 😅 I recently started thinking about an exciting new project and I can’t wait to share it with everyone.

<h2 class="h3">News</h2>

1. A lot of people still ask me about the printed version of the Better Web Type book. You can get a copy on [Amazon](https://www.amazon.com/gp/product/B074DJCP6F?pf_rd_p=2d1ab404-3b11-4c97-b3db-48081e145e35&pf_rd_r=MNRSHAAFWSSS00B6GE4G) or [Book Depository](https://www.bookdepository.com/Better-Web-Typography-for-Better-Web-Matej-Latin/9781999809522?ref=grid-view&qid=1562489330476&sr=1-1) but it’s the first edition.

<h2 class="h3">Featured</h2>

#### [Vertical rhythm in Sketch](https://betterwebtype.com/articles/2019/07/02/designing-with-a-baseline-grid-and-vertical-rhythm-in-sketch/?utm_source=newsletter&utm_medium=email&utm_campaign=vertical-rhythm-sketch-post)

I already wrote a lot on vertical rhythm in typography but this time I wanted to take a look at how vertical rhythm can be established and used in Sketch. In this post, I look at why working with a baseline grid makes sense, how to establish the vertical rhythm and how to align type and components to it.

<h2 class="h3">Font of the month</h2>

{% include post/image.html url="/assets/img/newsletter/011/font@2x.jpg" %}

#### [**Gangster Grotesk**](https://www.freshfonts.io/?ref=betterwebtype.com)

A typeface designed by Fresh Fonts, Gangster Grotesk is a contemporary typeface that combines a sharp contrast with angled terminal strokes that curve inward ever so slightly. These elements are simple but effective, lending the typeface character while being practically invisible at small sizes.

<h2 class="h3">Cool Articles</h2>

#### [Developing a robust font-loading strategy for CSS Tricks](https://www.zachleat.com/web/css-tricks-web-fonts/?ref=betterwebtype.com)

I loved going through this case study by Zach Leatherman where he looks at optimizing the font loading at CSS Tricks. How awesome is that?

#### [Optimizing Google Fonts performance](https://www.smashingmagazine.com/2019/06/optimizing-google-fonts-performance/?ref=betterwebtype.com)

I remember there was a similar article a while ago. But with Google Fonts being so popular, there’s always something to learn how to optimize their performance. This is a cool article about that by Danny Cooper.

#### [Truncating multi-line text with CSS](https://www.bram.us/2019/05/21/truncating-multi-line-text-with-css/70687/?ref=betterwebtype.com)

Truncating content has become so crucial on the web. And now it seems we’ll soon be able to truncate multiple lines of text. Bram Van Damme shows us how in his latest post.

<h2 class="h3">Did you know?</h2>  
<div class="group no-bottom-margin"> <div class="col-6 right-pad-20px no-bottom-margin"> <p>The proper italic style (bottom) has differently designed letters and is sloped at an angle that the type designer designed it. It’s ugly cousin “oblique” (top) is something that comes from forcing a regular font into italic by just sloping it. This also happens when we apply <code>font-style: italic;</code> to a font that doesn’t have an italic style. This practice should be avoided.</p> </div> <div class="col-3 no-bottom-margin"> <img src="/assets/img/newsletter/011/italic-oblique.jpg" /> </div></div>
__

That’s it for this month, see you in August! 👋

Ciao,<br>Matej