---
title: Cool new stuff in web typography in July ’20
layout: post-newsletter
title-short: July ’20
categories:
- Newsletter
tags:
- Free Font
date: 2020-07-15 15:45:24 +0200
author: matej
toc: true
intro: Lora is the featured font, we take a look Cooper Black and why it’s everywhere,
  as well as fonts in politics (again) and UI typography.
edition: '022'
image: "/assets/img/newsletter/022/post@2x.jpg"

---
How are you? For me, it’s just another busy summer. I don’t know why but it seems that summers are generally very productive when it comes to working on my projects. Right now, I’m working on wrapping up my new course [UX Buddy](https://uxbuddy.co/) until September as well as constantly continue to work on Better Web Type and these newsletters. I’ll finally take some time off then in September.

<h2 class="h3">News</h2>

**⭐️** The number of reviews of the **Better Web Type book** recently surpassed 100 and its rating still averages at a high 4.4! 😲 I wanted to take this opportunity to thank everyone who took their time to review my book. Reviews really help support this project so I truly appreciate it! If you haven’t reviewed the book yet, you can do it in less than a minute by just rating it on [Goodreads](https://www.goodreads.com/book/show/35857337-better-web-typography-for-a-better-web) (writing an actual review is not required). Thanks! 🙏

<h2 class="h3">Featured</h2>

#### Cooper Black: Why this font is everywhere

A short, 10-minute video about the story of a font that’s literally everywhere. Why, when and how did it become so popular? What’s its story? Take a look, it’s a fascinating story.  
  
[Read more →](https://www.youtube.com/watch?v=Zu91meda2I8&feature=emb_title)

<h2 class="h3">Font of the month</h2>

{% include post/image.html url="/assets/img/newsletter/022/font@2x.jpg" %}

#### **Lora**

Lora is a modern-looking serif font but with a flavour of calligraphy. In fact, authors even describe it as calligraphy inspired. What’s cool about that is the really nice italic style that comes with it. It Comes in 3 weights and each of them has the accompanying italic style. Oh yeah, it’s Open Source and the latest version includes the variable font too! 😍  
  
[Check it out →](https://github.com/cyrealtype/Lora-Cyrillic)

<h2 class="h3">Cool Articles</h2>

[Biden's new campaign typefaces are a pivot to the general election](https://yello.substack.com/p/bidens-new-campaign-typefaces-are)  
You know what’s cool about politics? Nothing. Except for articles that dive into typeface combinations for the presidential election campaigns. This one looks into how Biden’s camp switched from using one sans serif typeface to using a combination of two—a serif and a sans-serif.   
  
[T](https://hugogiraudel.com/2020/05/18/using-calc-to-figure-out-optimal-line-height/)[he details of UI typography](https://developer.apple.com/videos/play/wwdc2020/10175/)  
Apple’s 30-minute video guide to awesome typography. Worth a watch!  
  
[10 excellent font pairing tools for designers](https://dribbble.com/stories/2020/05/27/font-pairing-tools-for-designers)  
Font pairing is hard, not matter how much you read and learn about it. So when I found this list of cool resources, I knew I had to include it.

<h2 class="h3">New resources</h2>

[W](https://typography.guru/journal/awesome-catalina-fonts/)[p FOFT Loader](https://wordpress.org/plugins/wp-foft-loader/)  
If you’re using WordPress and you want to use the best font loading technique to improve the performance and perceived performance of your website use this plugin. It’s based on Zach Leatherman’s font loading approach. Recommended to my by Chris Zahller 👍  
  
[7](https://hugogiraudel.com/2020/05/18/using-calc-to-figure-out-optimal-line-height/)[0+ free fonts for designers](https://www.websiteplanet.com/blog/best-free-fonts/)  
Lora, the font featured in this edition is from this list. These are all fonts that you can use commercially and a lot of them are Open Source. Thanks to Fabiola Castro for recommending this resource to me 🙏

<h2 class="h3">Recommended reading</h2>

Do you work for a tech company and feel miserable but don’t really know why? It might be the company you work for. Check out this book, I really enjoyed this one! 👍👍👍

<blockquote class="instagram-media" data-instgrm-permalink="https://www.instagram.com/p/CCqGUefAEK7/?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="12" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/p/CCqGUefAEK7/?utm_source=ig_embed&amp;utm_campaign=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div> <div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;"> View this post on Instagram</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div></div></a><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/CCqGUefAEK7/?utm_source=ig_embed&amp;utm_campaign=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by matejlatin (@matejlatin)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2020-07-15T09:31:35+00:00">Jul 15, 2020 at 2:31am PDT</time></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script>