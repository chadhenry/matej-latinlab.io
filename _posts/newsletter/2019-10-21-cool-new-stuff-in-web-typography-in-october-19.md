---
layout: post-newsletter
title: Cool new stuff in web typography in October ’19
title-short: October ’19
categories:
- Newsletter
tags:
- Free-Font
- Variable-fonts
- Accessibility
date: 2019-10-21T14:06:57.000+00:00
author: matej
toc: true
intro: |2-

  Apple’s New York is the featured free font of this edition, we take a look at the Figma’s new support for OpenType, Google’s support for variable fonts and the story behind the creation of the Atkinson Hyperlegible typeface.
edition: '014'
image: "/assets/img/newsletter/014/post@2x.jpg"

---
How is it going? It’s been a really busy period for me and it will remain like that until the end of the year when I’ll finally take some time off again.

<h2 class="h3">News</h2>

1. [My book](https://betterwebtype.com/web-typography-book/) was featured in the post [Two books to start handling digital typography right](https://medium.muz.li/two-books-to-start-handling-digital-typography-right-cbef9342c640). I’m really glad to see my book bringing so much value to people 🎉

<h2 class="h3">Featured</h2>

#### An ode to OpenType: fall in love with the secret world of fonts

Figma was at the centre of last month’s featured post and it’s here again. This time, it’s a post introducing the new support for OpenType features in Figma. This will allow you to turn on additional ligatures, customize the style of digits, switch to alternative letterforms, and make use of adjustments available in many of today’s fonts — through a user interface that will make discovering them as much fun as using them.

[Check it out →](https://www.figma.com/blog/opentype-font-features/?ref=betterwebtype)

<h2 class="h3">Font of the month</h2>

{% include post/image.html url="/assets/img/newsletter/014/font@2x.jpg" %}

#### **New York**

This is actually a font that Apple introduced on the Macintosh back in 1983. It’s been tucked away for many years since then but it’s coming into the spotlight again. It’s supposed to work well on its own or combined with Apple’s main system font—San Francisco. You can get it for free right now, I don’t think you can use it for commercial purposes though.

[Check it out →](https://developer.apple.com/fonts/)

<h2 class="h3">Cool Articles</h2>

#### [V](https://css-tricks.com/styling-links-with-real-underlines/?ref=betterwebtype)[ariable fonts & the new Google fonts API](https://rwt.io/typography-tips/variable-fonts-new-google-fonts-api?ref=betterwebtype)

Google Fonts is introducing variable fonts support and changing the API for loading the fonts. [Read more →](https://rwt.io/typography-tips/variable-fonts-new-google-fonts-api?ref=betterwebtype)

#### [T](https://johndjameson.com/blog/kerning-and-ligatures-in-letterspaced-type/?ref=betterwebtype)[his typeface hides a secret in plain sight. And that’s the point](https://www.fastcompany.com/90395836/this-typeface-hides-a-secret-in-plain-sight-and-thats-the-point?ref=betterwebtype)

An interesting case study of how Atkinson Hyperlegible typeface was created. The designers broke a lot of rules while creating it but it resulted in a typeface that’s easy to read even to people with visual impairment. [Read more →](https://www.fastcompany.com/90395836/this-typeface-hides-a-secret-in-plain-sight-and-thats-the-point?ref=betterwebtype)

#### [R](https://www.fastcompany.com/90393259/ikea-is-quietly-changing-its-brand-again-for-a-very-good-reason?ref=betterwebtype)[eviving a blackletter font from a museum’s archive](https://typography.guru/journal/blackletter-revival/?ref=betterwebtype)

Another interesting case study on typeface design, but this time, it’s a revival of a 1916 blackletter typeface. [Read more →](https://typography.guru/journal/blackletter-revival/?ref=betterwebtype)

#### [T](https://css-irl.info/variable-font-animation-with-css-and-splitting-js/?ref=betterwebtype)[hese font football kits are perfect for type nerds](https://www.creativebloq.com/news/typekits?ref=betterwebtype)

This is so much fun. If famous typefaces were football kits, what would they look like? Take a look! If these were real football clubs, which one would you want to play for? [Read more →](https://www.creativebloq.com/news/typekits?ref=betterwebtype)

<h2 class="h3">Did you know?</h2>  
<div class="group no-bottom-margin"> <div class="col-6 right-pad-20px no-bottom-margin"> <p>Futura was the typeface used on the commemorative plaque on the Apollo 11 landing module so it’s literally the first typeface that made it to the moon (and remained there).</p> </div> <div class="col-3 no-bottom-margin"> <img src="/assets/img/newsletter/014/moon.jpg" /> </div></div>

<h2 class="h3">Photo of the month</h2>  
Can you spot the bad kerning on this photo? [@matejlatin](https://www.instagram.com/matejlatin/).

<blockquote class="instagram-media" data-instgrm-permalink="https://www.instagram.com/p/B3wF8pjBS2G/?utm_source=ig_embed&utm_campaign=loading" data-instgrm-version="12" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/p/B3wF8pjBS2G/?utm_source=ig_embed&utm_campaign=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div> <div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;"> View this post on Instagram</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div></div></a><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/B3wF8pjBS2G/?utm_source=ig_embed&utm_campaign=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">A post shared by matejlatin (@matejlatin)</a> on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2019-10-18T07:38:26+00:00">Oct 18, 2019 at 12:38am PDT</time></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script>
__

That’s it for this month, see you in November! 👋

Cheers,<br>Matej