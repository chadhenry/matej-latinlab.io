---
title: Cool new stuff in web typography in August ’20
layout: post-newsletter
title-short: August ’20
categories:
- Newsletter
tags:
- Free Font
date: 2020-08-19T15:45:24.000+02:00
author: matej
toc: true
intro: League Mono is the featured font, we take a look at getting the most out of
  Google variable fonts, modern CSS techniques for improving legibility and Gill Sans.
edition: '023'
image: "/assets/img/newsletter/023/post@2x.jpg"

---
I can’t believe it’s already one month since my last newsletter. Time passes so quickly, especially when I’m working so hard and rushing to launch [UX Buddy](https://uxbuddy.co/) in September. Luckily, there are only 3 more weeks until I take some time off and rest a bit.

<h2 class="h3">News</h2>

🎁 I organised a giveaway to celebrate the 3rd anniversary of the Better Web Type Book. I promised I would give away 3 printed copies of the special edition of the book to 3 randomly chosen customers that buy the digital version. Here are the winners: **Davide Quaranta**, **Niklas Stiller** and **Belinda White**. 🥳 Congratulations! I’ll be in touch to get your addresses.

📖 With such high demand for the printed version of the book, I decided to find a way to make the high-quality print edition available again. Stay tuned, I’ll send out an update out soon.

<h2 class="h3">Featured</h2>

#### Getting the most out of variable fonts on Google fonts

An article by Stephen Nixon for CSS Tricks on variable fonts. A must-read!

[Read more →](https://css-tricks.com/getting-the-most-out-of-variable-fonts-on-google-fonts/)

<h2 class="h3">Font of the month</h2>

{% include post/image.html url="/assets/img/newsletter/023/font@2x.jpg" %}

#### **League Mono**

This monospaced font is somewhat unusual as it comes in eight weights. Each of them looks really good and the font itself also looks awesome if used in a code editor.

[Check it out →](https://www.theleagueofmoveabletype.com/league-mono)

<h2 class="h3">Cool Articles</h2>

[Modern CSS techniques to improve legibility](https://www.smashingmagazine.com/2020/07/css-techniques-legibility/)  
Edoardo Cavazza wrote an exhaustive guide to improving legibility with the latest CSS techniques. It takes a while to get through the article but it’s well worth it.

[The fastest Google fonts](https://csswizardry.com/2020/05/the-fastest-google-fonts/)  
Harry Roberts brings his typically analytical approach to making Google fonts even faster.

[Eric Gill got it wrong; a re-evaluation of Gill Sans](https://www.typotheque.com/articles/re-evaluation_of_gill_sans)  
We recently compared Gill Sans and Johnston in a puzzle. This article goes beyond the most obvious differences between the two typefaces.

[A step-by-step guide for pairing fonts](https://learnui.design/blog/guide-pairing-fonts.html)  
Here’s an alternative approach to combining fonts. It’s a bit different from what I suggested in mine but an interesting approach nevertheless.

<h2 class="h3">New resources</h2>

[Font in Logo](https://www.fontinlogo.com/)  
A website where you can learn what font was used for famous logos.

[Programming fonts](https://www.programmingfonts.org/)  
Preview 98 programming fonts to choose the one you like the best. My favourite is still JetBrains Mono 😜