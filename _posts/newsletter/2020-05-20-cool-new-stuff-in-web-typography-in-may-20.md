---
title: Cool new stuff in web typography in May ’20
layout: post-newsletter
title-short: May ’20
categories:
- Newsletter
tags:
- Free Font
date: 2020-05-20T13:45:24.000+00:00
author: matej
toc: true
intro: Scunthorpe Sans is the featured font, we take a look at using fonts for complex
  data, as well as micro typography, font combinations, how fonts influence us, and
  fonts in popular things.
edition: '020'
image: "/assets/img/newsletter/020/post@2x.jpg"

---
How’s it going? Life is slowly getting back to normal here in Slovenia—finally! I was recently able to take some time off and rest so now I’m back refreshed and enthusiastic about work again. We have a lot of catching up to do on web typography related stuff so let’s dive right in!

<h2 class="h3">News</h2>

**📚** Jordan Hughes, the guy behind the Good Books website recently got in touch and told me that he added [my book recommendations](https://www.goodbooks.io/people/matej-latin), as well as [my book](https://www.goodbooks.io/books/better-web-typography-for-a-better-web) to a couple of pages on it. Thanks, Jordan! 🙇‍♂️  
  
**👨‍💻** I just finished writing the first draft for my new course [UX Buddy](https://uxbuddy.co/). The next stage is editing and proofreading to iron out all the details. After that, I’ll finally put it online somehow—I’m not yet sure of the actual implementation. Still hoping for an early-stage release in September.

<h2 class="h3">Featured</h2>

#### Fonts for complex data

Have you ever had to design a dashboard or a chart? Or maybe even a dashboard of charts? I was presented with challenges like this in the past and always felt frustrated because of a lack of resources or knowledge when it came to fonts. My work sometimes looked strange, but I never knew exactly why. In this article, Jonathan Hoefler gives solid and sound advice on how to use fonts for showing complex data. It’s a must-read!

[Read more →](https://www.typography.com/blog/fonts-for-complex-data)

<h2 class="h3">Font of the month</h2>

{% include post/image.html url="/assets/img/newsletter/020/font@2x.jpg" %}

#### **Scunthorpe Sans**

Scunthorpe is a small town in England that I used to pass on my train travels to Edinburgh. Nothing about it stands out. But now there’s a font named after it. And it’s a really interesting font because it automatically censors some of the nasty words in the English language. The authors of the font used ligatures to do it—that’s genius! I love the presentation on the website as well!

[Check it out →](https://vole.wtf/scunthorpe-sans/)

<h2 class="h3">Cool Articles</h2>

[Micro-Typography: How To Space And Kern Punctuation Marks And Other Symbols](https://www.smashingmagazine.com/2020/05/micro-typography-space-kern-punctuation-marks-symbols/)  
We already know that there are different sizes of space in typography, but here’s a really cool article that will teach you how and when to use which one.   
  
[How typefaces influence you](https://www.fastcompany.com/90502503/how-typefaces-influence-you)  
There’s existing research that suggests that typefaces can have a significant impact on our perception. Here’s (another) article looking at typefaces used in politics.  
  
[Typographic doubletakes](https://www.typography.com/blog/typographic-doubletakes)  
We covered the basics of combining typefaces in the Better Web Type course. Jonathan Hoefler takes a look at five more ways of finding cool and original combinations.   
  
[The fonts in popular things identified Vol. 1](https://www.typewolf.com/blog/fonts-in-popular-things-identified-vol-1)  
Have you ever watched a series on Netflix and wondered what font they used for the logo or the credits? I know I have, it happens to me all the time. Apparently, this happens to Jeremiah Shoaf from Typewofl too as he just started a new series of blog posts about it.