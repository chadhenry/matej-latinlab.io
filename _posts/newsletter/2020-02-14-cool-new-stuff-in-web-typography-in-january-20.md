---
title: Cool new stuff in web typography in February ’20
layout: post-newsletter
title-short: February ’20
categories:
- Newsletter
tags:
- Free Font
- Coding
date: 2020-01-20T14:45:24.000+00:00
author: matej
toc: true
intro: 'Public Sans is the featured font, we take a look at 5 monospaced fonts with
  cool coding ligatures and how type affects politics. '
edition: '017'
image: "/assets/img/newsletter/017/post@2x.jpg"

---
<h2 class="h3">News</h2>

❤️ Today is exactly 3 years since I launched the free Better Web Type course. More than **30k people signed up** for it and its rating is still an unbelievably high one—**4.7** after more than **1,000 reviews**! Thanks, everyone for supporting my work!  
**🤔** I’m trying out something new in this issue to make it more fun. I’m including a **puzzle/question** to test your knowledge about typography. It’s at the beginning of this email but the answer is at the bottom. This should give you enough time to think about your answer.

<h2 class="h3">Featured</h2>

#### 5 monospaced fonts with cool coding ligatures

I’d always used the Monaco font and Sublime Text for coding but I recently decided to switch to VS Code. That change also led me to explore monospaced fonts suitable for coding as I worked on customising my VS Code theme.

[Check it out →](https://betterwebtype.com/articles/2020/02/13/5-monospaced-fonts-with-cool-coding-ligatures/)

<h2 class="h3">Font of the month</h2>

{% include post/image.html url="/assets/img/newsletter/017/font@2x.jpg" %}

#### **Public Sans**

Public Sans is the official font of the digital.gov—United States’ website for technology and information services. It seems they designed this awesome font which is free for personal _and_ commercial use. It comes in 9 weights, all with italic equivalents 😲 This makes it a very versatile font. It looks really good too!

[Check it out →](https://public-sans.digital.gov/)

<h2 class="h3">Cool Articles</h2>

[The real politics of type](https://www.fastcompany.com/90458816/the-real-politics-of-type)  
I don’t like politics but I love to see any research about typography. This one is about people making assumptions about candidates based on the typefaces they use for their branding.   
  
[Set type on a circle... with offset-path](https://css-tricks.com/set-type-on-a-circle-with-offset-path/)  
Another cool tutorial by Chris Coyier. This time he shows us how to set type on a circle with CSS alone.  
  
[Here’s the typography of the next decade](https://theoutline.com/post/8385/didones-font-of-the-decade)  
What will typography be like in the next decade? Are geometric sans-serif typefaces going to continue to dominate? Rachel Hawley shares her thoughts.

<h2 class="h3">Did you know?</h2>  
<div class="group no-bottom-margin"> <div class="col-6 right-pad-20px no-bottom-margin"> <p>The special thing about tabular figures is that they all have the same width. The width of the number “1” equals the width of the “0” for example. Not the actual glyph, but including the spacing around it. The uniform spacing makes them great for aligning them vertically in tables or generally in columns of numbers.</p> </div> <div class="col-3 no-bottom-margin"> <img src="/assets/img/newsletter/017/tabular-answer.jpg" /> </div></div>