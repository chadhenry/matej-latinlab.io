---
layout: post-newsletter
title: Cool new stuff in web typography in November ’19
title-short: November ’19
categories:
- Newsletter
tags:
- Free-Font
- Branding
date: 2019-11-19T15:06:57.000+00:00
author: matej
toc: true
intro: |2-

  Hellvetica is the featured free font of this edition, we take a look at typographic illusions, accessible dropcaps as well as words and phrases that originated in the field of typography.
edition: '015'
image: "/assets/img/newsletter/015/post@2x.jpg"

---
How is it going? I just wrapped up a part of the work for my new project but I’m not taking any rest just yet. There’s more work I want to still do by the end of this year.

<h2 class="h3">News</h2>

1. Last month I launched the website for my new project—**UX Buddy**. If you’re a designer, looking to take the next steps in your career, you should definitely [check it out](https://uxbuddy.co/?ref=bwt-newsletter-15).

<h2 class="h3">Featured</h2>

#### How to choose a font for a project

I recently received an email from a designer called Jared. He went through the typography lessons that I offer as part of the free course and he was very grateful for them. He said he learned a lot but also had one important question: how do I go about choosing a font for my project?

[Check it out →](https://betterwebtype.com/articles/2019/10/20/how-to-choose-a-font-for-a-project/)

<h2 class="h3">Font of the month</h2>

{% include post/image.html url="/assets/img/newsletter/015/font@2x.jpg" %}

#### **Hellvetica**

Well yes, this is clearly a joke. But I love how well it’s made and presented. It reminds me so much of [this](https://www.google.com/search?q=how+to+give+your+designer+friends+a+migraine&sxsrf=ACYBGNShnCBrW-KAnAbZx_l2iA1_7MvJNQ:1573745877517&source=lnms&tbm=isch&sa=X&ved=0ahUKEwi2zfqfhOrlAhWFpIsKHZ7CBtoQ_AUIEigB&biw=1680&bih=916#imgrc=mXzT54tm4rkiRM:). I wonder if there can be some other use for this font, other than this mockery of course 😂

[Check it out →](https://hellveticafont.com/)

<h2 class="h3">Cool Articles</h2>

#### [Words and phrases in common use which originated in the field of typography](https://typography.guru/journal/words-and-phrases-in-common-use-which-originated-in-the-field-of-typography-r78/)

Five phrases that originated in typography. The last one from this list is the coolest and most surprising one. [Read more →](https://typography.guru/journal/words-and-phrases-in-common-use-which-originated-in-the-field-of-typography-r78/)

#### [Typographic illusions](https://www.typography.com/blog/typographic-illusions)

This is a really interesting link about optical details in typefaces. [Read more →](https://www.typography.com/blog/typographic-illusions)

#### [Accessible drop caps](https://adrianroselli.com/2019/10/accessible-drop-caps.html)

This is how you do drop caps right. Adrian Roselli goes into details and even tests different implementations with the screen reader. [Read more →](https://adrianroselli.com/2019/10/accessible-drop-caps.html)

<h2 class="h3">New resources</h2>

[T](https://typography.guru/journal/words-and-phrases-in-common-use-which-originated-in-the-field-of-typography-r78/)[ransfonter](https://transfonter.org/)  
Modern and simple css @font-face generator.  
  
[W](https://www.typography.com/blog/typographic-illusions)[akamai Fondue](https://wakamaifondue.com/)  
Interesting name, cool tool. Wakamai Fondue shows which features a font supports. Just drag and drop your font.  
  
[f](https://adrianroselli.com/2019/10/accessible-drop-caps.html)[onttools](https://github.com/fonttools/fonttools)  
A library to manipulate font files from Python.

<h2 class="h3">Did you know?</h2>  
<div class="group no-bottom-margin"> <div class="col-6 right-pad-20px no-bottom-margin"> <p>Arial is a copy of Helvetica and is often referred to as “Helvetica’s ugly twin”. It was created to be metrically identical, with all character widths identical, so that a document designed in Helvetica could be displayed and printed correctly without having to pay for a Helvetica license.</p> </div> <div class="col-3 no-bottom-margin"> <img src="/assets/img/newsletter/015/arial.jpg" /> </div></div>
__

That’s it for this month, see you in December! 👋

Cheers,<br>Matej