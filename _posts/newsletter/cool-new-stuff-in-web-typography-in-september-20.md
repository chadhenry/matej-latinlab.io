---
title: Cool new stuff in web typography in September ’20
layout: post-newsletter
title-short: September ’20
categories:
- Newsletter
tags: []
date: 2020-09-14 15:45:24 +0200
author: matej
toc: true
intro: Signifier is the featured font, we take a look at a typographic redesign, improving
  the iOS time display as well as a cool selection of Google Fonts.
edition: '024'
image: "/assets/img/newsletter/024/post@2x.jpg"

---
I’m back from a one-week break. Just a quick one to catch my breath before the final push this year. Still so much more to do and so many things I’m excited about—my new course to help designers find better jobs, [UX Buddy](https://uxbuddy.co/), being the main one. I’ve been working on it for more than a year now. After such a long marathon, I can finally see the finish line!

<h2 class="h3">News</h2>

🎉 This is the 24th edition which means that it’s been two years since I started this newsletter! Holy shit, I really can’t believe how time flies, especially when doing stuff that you love. Huge thanks to everyone sending me support and love. It helps me keep going in times when I’m short on time, or energy.  
  
📣 Many have been telling me lately that my newsletter often goes into their Promo, or maybe even the Spam folder. If that’s the case, you can manually move it to the main inbox folder and the future editions should go there straight away 🤞

<h2 class="h3">Featured</h2>

#### These are the fonts behind 30 famous logos

I always enjoy these types of articles, it’s just so fun to see how the typefaces shape our perception of the world around us without us even knowing it. There’s a reason Futura, Helvetica and Avant-Garde are the most commonly used fonts for iconic logos but we never really think about it. Anyway, enjoy the quick read!  
  
[Read more →](https://uxdesign.cc/these-are-the-fonts-behind-30-famous-logos-40c83cd05d16)

<h2 class="h3">Font of the month</h2>

{% include post/image.html url="/assets/img/newsletter/024/font@2x.jpg" %}

#### **Signifier**

I loved [the article](https://klim.co.nz/blog/signifier-design-information/) explaining how this font was made and where it comes from that I decided to feature it as the font of the month even though it’s not free. Well, you can test it for free but the test version doesn’t have any OpenType features. Try it out, if you like it and decide that you’ll use it, buy it. Too often we expect high-quality fonts to be free. We need to keep in mind that an enormous amount of work goes into producing these. Let’s support type foundries whenever we can.  
  
[Check it out →](https://klim.co.nz/retail-fonts/signifier/)

<h2 class="h3">Cool Articles</h2>

[Redesigning the Almanack of Naval](https://youtu.be/v7NnSibhj7w)  
Erik Kennedy from LearnUI reached out and told me he recently did a redesign that’s heavy on typography and video documented it. A lot to learn from a 10-minutes video.  
  
[10 rules to help you rule type](https://www.youtube.com/watch?v=QrNi9FmdlxY&ab_channel=TheFutur&t=1s)  
Another short video (3 mins) and a bit older, but I never saw it until recently. It covers the basics but I just love the way it’s animated. 👏  
  
[Improving the typography of the iOS time display](https://www.zeichenschatz.net/typografie/improving-the-typography-of-the-ios-time-display.html)  
This is an interesting one. I never paid so much attention to the time display on my iPhone but I’m glad that Oliver Schöndorfer did.   
  
[Cool Modular Signage for the National Library of Luxembourg](https://kottke.org/20/09/cool-modular-signage-for-the-national-library-of-luxembourg?ref=sidebar)  
This is awesome! A system of blocks that make up all the letters and other symbols. Each letter is made up of either 6 or 9 separate blocks. And it looks so neat.

<h2 class="h3">New resources</h2>

[A selection of Google Fonts](https://www.figma.com/community/file/843916328104333682)  
A selection of high-quality fonts available on Google Fonts by Maxime Rabot.