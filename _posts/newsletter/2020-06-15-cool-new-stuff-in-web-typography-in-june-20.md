---
title: Cool new stuff in web typography in June ’20
layout: post-newsletter
title-short: June ’20
categories:
- Newsletter
tags:
- Free Font
date: 2020-06-15T15:45:24.000+02:00
author: matej
toc: true
intro: Scunthorpe Sans is the featured font, we take a look at using fonts for complex
  data, as well as micro typography, font combinations, how fonts influence us, and
  fonts in popular things.
edition: '021'
image: "/assets/img/newsletter/021/post@2x.jpg"

---
How’s it going? I just came back from a short break I took to visit my family. Just a couple of days to disconnect and recharge, no time for longer breaks until September. There’s still so much work to do before then.

<h2 class="h3">Featured</h2>

#### YouTube Sans: The making of a typeface

I’m not sure how old this case study is but I only stumbled upon it today. I love such case studies and I particularly like how this one is presented with animations.   
  
[Read more →](https://design.google/library/youtube-sans-the-making-of-a-typeface/)

<h2 class="h3">Font of the month</h2>

{% include post/image.html url="/assets/img/newsletter/021/font@2x.jpg" %}

#### **Jost**

This is a font that I stumbled upon just today. It’s on Google Fonts and it’s a variable font! I loved the light retro geometric sans feel it has to it. Can’t wait to use this one in a project! 🤩  
  
[Check it out →](https://fonts.google.com/specimen/Jost?vfonly)

<h2 class="h3">Cool Articles</h2>

[The awesome Mac OS Catalina fonts you didn’t know you had access to](https://typography.guru/journal/awesome-catalina-fonts/)  
That’s right. You can download a bunch of additional cool fonts on Mac OS.   
  
[Using calc to figure out optimal line-height](https://hugogiraudel.com/2020/05/18/using-calc-to-figure-out-optimal-line-height/)  
Jesús Ricarte looks into finding the optimal line-height by using calc. His solution looks really good in the end.  
  
[10 excellent font pairing tools for designers](https://dribbble.com/stories/2020/05/27/font-pairing-tools-for-designers)  
Font pairing is hard, not matter how much you read and learn about it. So when I found this list of cool resources, I knew I had to include it.