---
layout: book
title: Better Web Typography for a Better Web
permalink: /web-typography-book/
exclude: true
body-class: "book-page"
comments: false
active: book
rating: 4.4
reviews: 107
alert: 🎉 3rd anniversary of the book — get 30% off, <b>applied automatically</b> at checkout for a limited time!
---
