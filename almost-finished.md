---
layout: page-bare
title: Almost finished
permalink: /almost-finished/
intro: Hey, I just need to confirm your email so please click on the link that was just sent to your email.
custom-js: /assets/js/sniper-email.js
---

<div class="email-link">
</div>
