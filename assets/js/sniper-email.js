// Get data from URL
function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
  results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

var varEmail = getParameterByName('email');

if ( varEmail.includes('gmail') ) {
  $(".email-link").html(
    '<a href="https://mail.google.com/mail/u/0/#search/from%3A%40matejlatin.co.uk+in%3Aanywhere">Check your inbox</a>'
  );
}
