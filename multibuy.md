---
layout: multibuy
title: eBook multibuy
permalink: /multibuy/
comments: false
---

{%- if site.posts.size > 0 -%}
  <h2 class="h4">{{ page.list_title | default: "Past Editions" }}</h2>
  <div class="group">
    <div class="post-list">
      <hr>
      <ul class="">
        {%- for post in site.categories['Newsletter'] -%}
          {% unless post.exclude %}
            {%- include post-list.html type="specific" intro="yes" -%}
          {% endunless %}
        {%- endfor -%}
      </ul>
    </div>
  </div>
{%- endif -%}

