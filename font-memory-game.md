---
layout: font-memory-game
title: Font Memory Game
permalink: /font-memory-game/
exclude: true
custom-js: /assets/js/memory-game.js
image: /assets/img/memory-game/font-memory-game-dark.jpg
body-class: quiz-page
---
