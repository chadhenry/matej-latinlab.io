---
layout: font-memory-game-hard
title: Font Memory Game
permalink: /font-memory-game-hard/
exclude: true
custom-js: /assets/js/memory-game-hard.js
image: /assets/img/memory-game/font-memory-game-dark.jpg
body-class: quiz-page
---
