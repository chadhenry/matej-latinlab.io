**Send date: [mmm dd, yyyy]**


Checklist:
* [ ] Non-customers campaign
* [ ] Customers campaign
* [ ] Blog post
* [ ] [Create an issue](https://gitlab.com/matej-latin/matej-latin.gitlab.io/-/issues/new) for next month

### Open loop

Add an open loop to the intro text (from "did you know?" section)

<img src="/uploads/d039f0b773207a6066007a3eb652b3d7/image.png" width="400" />

### News

- Donations
- News item

### Featured

- Featured article

### Font of the month

- Font

### The puzzle

* Who's the typeface designer with most typefaces designed?
* Can you name the "web safe" fonts that were the only font options in the beginning of the internet?
* What is the style of this typeface?
* Fake or real? (Small caps, italics, bolds etc.)
* What is the style of these numbers (proportional, tabular, lining) - https://www.fonts.com/content/learning/fontology/level-3/numbers
* REM vs EM calculation

<img src="/uploads/e5b6c7617e14e5953d1984048c5b2d60/image.png" width="400" />

https://medium.com/the-mission/how-morning-brews-referral-program-built-an-audience-of-1-5-million-subscribers-3315482c1aa5

*From https://www.wired.com/2010/08/the-itch-of-curiosity/*

> The first thing the scientists found is that curiosity obeys an inverted U-shaped curve, so that we're most curious when we know a little about a subject (our curiosity has been piqued) but not too much (we're still uncertain about the answer). 

### New resources

- Resource

### Articles

- Articles

### Did you know?

- An interesting fact about typography

Ideas

* Display vs text typefaces
* Grotesuq name origin
* Futura on the moon
* Arial is a copy of Helvetica
* Serif vs sans-serif readability myth

### Book ad

Book update, an interesting fact, funny story, insider knowledge.

[700+ Power Words That Will Boost Your Conversions](https://optinmonster.com/700-power-words-that-will-boost-your-conversions/)

<img src="/uploads/3c1e29b50c44e88c216e31137160ec0e/image.png" width="400" />

### Forward this newsletter to a friend

---

**Content ideas:**

- inspiration/gallery
- featured top donor
- cool new fonts
- added new resources
- quick tips
- feature book reviews
- sketch tips

**Sources:**

- https://www.typemag.org/
- [variable font articles at the bottom](https://blog.prototypr.io/an-exploration-of-variable-fonts-37f85a91a048)
- https://fonts.google.com/specimen/IBM+Plex+Sans

/label ~Newsletter ~Campaign ~Traffic