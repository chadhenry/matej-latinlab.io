- Check topic popularity with https://buzzsumo.com/

---

### Post ideas

- Typography book reviews
- Newsletters
- Inspiration/teardowns (examples of websites with great typography and why it works)
- Typography for design systems articles
  - Vertical rhythm for web typography in web applications
- Posts about cool fonts ('Awesome geometric typefaces')
- Tools (and how to use them)
- Guides 

---

### Post title: 

**Short description**: 

#### Articles & resources

- item

---

### Submit

* [ ] [Sidebar.io](http://sidebar.io)
* [ ] Prototypr.io
* [ ] [Muzli app](https://muz.li/contact/)
* [ ] [Webdesignernews.com](http://webdesignernews.com)
* [ ] [Web designer depot](https://www.webdesignerdepot.com/contact/)
* [ ] [Hacker news](https://news.ycombinator.com/submit)
* [ ] [Designernews.co](http://designernews.co)
* [ ] [heydesigner.com](https://discord.gg/6sXuh4B)
* [ ] [Speckyboy](https://speckyboy.com/contact/)
* [ ] [uxdesignweekly.com](http://uxdesignweekly.com/contact/)
* [ ] [css weekly](https://css-weekly.com/submit-a-link/)
* [ ] [Frontend Front](https://frontendfront.com/)

/label ~Blog ~Traffic ~Aggregators ~SEO ~Campaign
/assign @matejlatin 