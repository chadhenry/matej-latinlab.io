---
layout: page
title: Articles
permalink: /articles/
exclude: true
intro: All web typography articles.
---

{%- if site.posts.size > 0 -%}
  <div class="group">
    <div class="post-list">
      <hr>
      <ul class="">
        {%- for post in site.categories['Articles'] -%}
          {% unless post.exclude %}
            {%- include post-list.html type="specific" intro="yes" -%}
          {% endunless %}
        {%- endfor -%}
      </ul>
    </div>
  </div>
{%- endif -%}
